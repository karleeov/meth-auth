import dotenv from 'dotenv'
import  express  from 'express'

dotenv.config()

const port = process.env.PORT || 5000
import userRoutes from './routes/userRoutes.js'


app.use('/api/users', userRoutes)
const app = express()

app.get('/', (req,res) => res.send('server running'))

app.listen(port,() => console.log(`runnning ${port}`))

